﻿using MRTSharp.Model.IP;
using System;
using System.Net;
using System.Net.Sockets;
using Xunit;

namespace MRTSharp.Test.Model.IP
{
	public class IPPrefixTest
	{
		[Fact]
		public void TestGetNetworkAddress_Normalv4()
		{
			IPPrefix prefix = IPPrefix.Parse("192.168.0.0/24");

			Assert.NotNull(prefix);

			Assert.Equal(IPAddress.Parse("192.168.0.0"), prefix.GetNetworkAddress());
		}

		[Fact]
		public void TestEquals_SameReferencev4()
		{
			IPPrefix prefix1 = IPPrefix.Parse("192.168.0.0/24");
			IPPrefix prefix2 = prefix1;

			Assert.NotNull(prefix1);
			Assert.NotNull(prefix2);

			Assert.Equal(prefix1.GetHashCode(), prefix2.GetHashCode());
			Assert.True(prefix1.Equals(prefix2));
		}

		[Fact]
		public void TestEquals_EqualObjectv4()
		{
			IPPrefix prefix1 = IPPrefix.Parse("192.168.0.0/24");
			IPPrefix prefix2 = IPPrefix.Parse("192.168.0.0/24");

			Assert.NotNull(prefix1);
			Assert.NotNull(prefix2);

			Assert.Equal(prefix1.GetHashCode(), prefix2.GetHashCode());
			Assert.True(prefix1.Equals(prefix2));
		}

		[Fact]
		public void TestEquals_NotEqualPrefixv4()
		{
			IPPrefix prefix1 = IPPrefix.Parse("192.168.0.0/24");
			IPPrefix prefix2 = IPPrefix.Parse("192.168.161.0/24");

			Assert.NotNull(prefix1);
			Assert.NotNull(prefix2);

			Assert.NotEqual(prefix1.GetHashCode(), prefix2.GetHashCode());
			Assert.False(prefix1.Equals(prefix2));
		}

		[Fact]
		public void TestEquals_NotEqualCIDRv4()
		{
			IPPrefix prefix1 = IPPrefix.Parse("192.168.0.0/24");
			IPPrefix prefix2 = IPPrefix.Parse("192.168.0.0/25");

			Assert.NotNull(prefix1);
			Assert.NotNull(prefix2);

			Assert.NotEqual(prefix1.GetHashCode(), prefix2.GetHashCode());
			Assert.False(prefix1.Equals(prefix2));
		}

		[Fact]
		public void TestEquals_EqualNullv4()
		{
			IPPrefix prefix1 = IPPrefix.Parse("192.168.0.0/24");

			Assert.NotNull(prefix1);

			Assert.False(prefix1.Equals(null));
		}

		[Fact]
		public void TestEquals_EqualObjectGenericv4()
		{
			IPPrefix prefix1 = IPPrefix.Parse("192.168.0.0/24");
			Object prefix2 = IPPrefix.Parse("192.168.0.0/24");

			Assert.NotNull(prefix1);
			Assert.NotNull(prefix2);

			Assert.Equal(prefix1.GetHashCode(), prefix2.GetHashCode());
			Assert.True(prefix1.Equals(prefix2));
		}

		[Fact]
		public void TestParse_Normalv4()
		{
			IPPrefix prefix = IPPrefix.Parse("192.168.0.0/24");

			Assert.NotNull(prefix);

			Assert.Equal(24, prefix.Cidr);
			Assert.Equal(new byte[] { 192, 168, 0 }, prefix.Prefix);
			Assert.Equal(AddressFamily.InterNetwork, prefix.Family);
		}

		[Fact]
		public void TestParse_Normalv6()
		{
			IPPrefix prefix = IPPrefix.Parse("25e3::/24");

			Assert.NotNull(prefix);

			Assert.Equal(24, prefix.Cidr);
			Assert.Equal(new byte[] { 37, 227, 0 }, prefix.Prefix);
			Assert.Equal(AddressFamily.InterNetworkV6, prefix.Family);
		}

		[Fact]
		public void TestParse_NoCIDRv4()
		{
			FormatException exception = Assert.Throws<FormatException>(() => IPPrefix.Parse("192.168.0.0"));

			Assert.Equal("CIDR not defined.", exception.Message);
		}

		[Fact]
		public void TestParse_NoCIDRv6()
		{
			FormatException exception = Assert.Throws<FormatException>(() => IPPrefix.Parse("::1"));

			Assert.Equal("CIDR not defined.", exception.Message);
		}

		[Fact]
		public void TestToString_Normalv4()
		{
			String prefixString = "192.168.0.0/24";
			IPPrefix prefix = IPPrefix.Parse(prefixString);

			Assert.NotNull(prefix);

			Assert.Equal(prefixString, prefix.ToString());
		}

		[Fact]
		public void TestConstructor_NoPrefix()
		{
			ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => new IPPrefix(null, 30, AddressFamily.InterNetwork));

			Assert.Equal("Value cannot be null. (Parameter 'prefix')", exception.Message);
		}

		[Fact]
		public void TestConstructor_WrongCIDRv4()
		{
			FormatException exception = Assert.Throws<FormatException>(() => new IPPrefix(new byte[] { 172, 12 }, 36, AddressFamily.InterNetwork));

			Assert.Equal("CIDR not valid.", exception.Message);
		}

		[Fact]
		public void TestConstructor_WrongCIDRv6()
		{
			FormatException exception = Assert.Throws<FormatException>(() => new IPPrefix(new byte[] { 172, 12 }, 130, AddressFamily.InterNetworkV6));

			Assert.Equal("CIDR not valid.", exception.Message);
		}

		[Fact]
		public void TestConstructor_TooLongPrefixv4_1()
		{
			ArgumentException exception = Assert.Throws<ArgumentException>(() => new IPPrefix(new byte[] { 172, 12, 35, 12 }, 13, AddressFamily.InterNetwork));

			Assert.Equal("Prefix length is not compatible with CIDR.", exception.Message);
		}

		[Fact]
		public void TestConstructor_TooLongPrefixv4_2()
		{
			ArgumentException exception = Assert.Throws<ArgumentException>(() => new IPPrefix(new byte[] { 172, 12 }, 8, AddressFamily.InterNetwork));

			Assert.Equal("Prefix length is not compatible with CIDR.", exception.Message);
		}
	}
}
