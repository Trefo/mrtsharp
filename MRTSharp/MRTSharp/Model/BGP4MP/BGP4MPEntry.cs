﻿using MRTSharp.Model.BGP4MP.Constants;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.MRT;
using System;

namespace MRTSharp.Model.BGP4MP
{
	public abstract class BGP4MPEntry : MRTEntry
	{
		public BGPPeer Peers { get; set; }
		public BGP4MPSubtype Subtype { get; private set; }

		public BGP4MPEntry(BGP4MPSubtype subType)
		{
			Subtype = subType;
		}

		public Byte GetASLength()
		{
			return Subtype switch
			{
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4 or 
				BGP4MPSubtype.BGP4MP_STATE_CHANGE_AS4 or 
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL or 
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4_ADDPATH or 
				BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL_ADDPATH => 4,
				BGP4MPSubtype.BGP4MP_STATE_CHANGE or 
				BGP4MPSubtype.BGP4MP_MESSAGE or
				BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL or
				BGP4MPSubtype.BGP4MP_MESSAGE_ADDPATH or 
				BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL_ADDPATH => 2,
				_ => throw new Exception("BGP4MP Message with subtype code " + Subtype + " not defined"),
			};
		}

		public Boolean HasAddPath()
		{
			return Subtype == BGP4MPSubtype.BGP4MP_MESSAGE_ADDPATH ||
					Subtype == BGP4MPSubtype.BGP4MP_MESSAGE_AS4_ADDPATH ||
					Subtype == BGP4MPSubtype.BGP4MP_MESSAGE_LOCAL_ADDPATH ||
					Subtype == BGP4MPSubtype.BGP4MP_MESSAGE_AS4_LOCAL_ADDPATH;
		}
	}
}
