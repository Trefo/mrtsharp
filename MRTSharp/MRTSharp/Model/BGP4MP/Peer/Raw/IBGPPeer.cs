﻿using MRTSharp.Model.IP;
using System;

namespace MRTSharp.Model.BGP4MP.Peer.Raw
{
	public interface IBGPPeer
	{
		public UInt32 GetPeerAS();

		public UInt32 GetLocalPeerAS();

		public UInt16 GetInterfaceNumber();

		public IPType GetIPType();

		public void SetInterfaceNumber(UInt16 interfaceNumber);

		public void SetIPType(UInt16 ipType);
	}
}
