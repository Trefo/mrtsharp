﻿using MRTSharp.Model.BGP4MP.Peer.Raw;
using System;
using System.Collections.Generic;
using System.Net;

namespace MRTSharp.Model.BGP4MP.Peer
{
	public class BGPPeer : IEquatable<BGPPeer>
	{
		public IBGPPeer Peers { get; set; }
		public IPAddress PeerIPAddress { get; set; }
		public IPAddress LocalPeerIPAddress { get; set; }

		public override bool Equals(object obj)
		{
			return Equals(obj as BGPPeer);
		}

		public bool Equals(BGPPeer other)
		{
			return other != null &&
				   EqualityComparer<IBGPPeer>.Default.Equals(Peers, other.Peers) &&
				   EqualityComparer<IPAddress>.Default.Equals(PeerIPAddress, other.PeerIPAddress) &&
				   EqualityComparer<IPAddress>.Default.Equals(LocalPeerIPAddress, other.LocalPeerIPAddress);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(Peers, PeerIPAddress, LocalPeerIPAddress);
		}

		public static bool operator ==(BGPPeer left, BGPPeer right)
		{
			return EqualityComparer<BGPPeer>.Default.Equals(left, right);
		}

		public static bool operator !=(BGPPeer left, BGPPeer right)
		{
			return !(left == right);
		}
	}
}
