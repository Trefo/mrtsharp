﻿using MRTSharp.Comparers;
using System;

namespace MRTSharp.Model.BGP.PathAttributes
{
	public class ASEntry : IEquatable<ASEntry>
	{
		public UInt32[] ASNumbers { get; set; }
		public ASPathSegmentType SegmentType { get; set; }

		private static readonly ArrayEqualityComparer<UInt32> ASComparer = new();

		public bool IsAsSet()
		{
			return SegmentType == ASPathSegmentType.AS_SET;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as ASEntry);
		}

		public bool Equals(ASEntry other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return ASComparer.Equals(ASNumbers, other.ASNumbers) &&
				   SegmentType == other.SegmentType;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(ASComparer.GetHashCode(ASNumbers), SegmentType);
		}

		public override string ToString()
		{
			return "(SEGMENT TYPE: " + SegmentType + ") => " + String.Join(", ", ASNumbers);
		}
	}
}
