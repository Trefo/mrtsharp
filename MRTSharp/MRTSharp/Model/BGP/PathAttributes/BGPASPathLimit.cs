﻿using System;
using System.Runtime.InteropServices;

namespace MRTSharp.Model.BGP.PathAttributes
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct BGPASPathLimit : IEquatable<BGPASPathLimit>
	{
		public byte UpperBound { get; set; }
		public UInt32 ASNumber { get; set; }

		public override bool Equals(object obj)
		{
			return obj is BGPASPathLimit limit && Equals(limit);
		}

		public bool Equals(BGPASPathLimit other)
		{
			return ASNumber == other.ASNumber &&
				   UpperBound == other.UpperBound;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(ASNumber, UpperBound);
		}

		public static bool operator ==(BGPASPathLimit left, BGPASPathLimit right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(BGPASPathLimit left, BGPASPathLimit right)
		{
			return !(left == right);
		}
	}
}
