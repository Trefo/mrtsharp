using MRTSharp.Comparers;
using System;

namespace MRTSharp.Model.BGP.PathAttributes
{
	public class BGPExtendedCommunity : IBGPCommunity, IEquatable<BGPExtendedCommunity>
	{
		public Byte Type { get; set; }
		public Byte? SubType { get; set; }
		public Byte[] Value { get; set; }

		private static readonly ArrayEqualityComparer<Byte> ValueComparer = new();

		public override bool Equals(object obj)
		{
			return obj is BGPExtendedCommunity community && Equals(community);
		}

		public bool Equals(BGPExtendedCommunity other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return Type == other.Type &&
					SubType == other.SubType &&
					ValueComparer.Equals(Value, other.Value);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(Type, SubType, ValueComparer.GetHashCode(Value));
		}

		public static bool operator ==(BGPExtendedCommunity left, BGPExtendedCommunity right)
		{
			return left is not null &&
					left.Equals(right);
		}

		public static bool operator !=(BGPExtendedCommunity left, BGPExtendedCommunity right)
		{
			return !(left == right);
		}
	}
}