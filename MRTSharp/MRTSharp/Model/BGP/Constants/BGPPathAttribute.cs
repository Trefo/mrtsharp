﻿namespace MRTSharp.Model.BGP.Constants
{
	public enum BGPPathAttribute
	{
#pragma warning disable CA1707 // This are identifier as assigned by IANA
		//https://www.iana.org/assignments/bgp-parameters/bgp-parameters.xhtml
		ORIGIN = 1,
		AS_PATH = 2,
		NEXT_HOP = 3,
		MULTI_EXIT_DISC = 4,
		LOCAL_PREF = 5,
		ATOMIC_AGGREGATE = 6,
		AGGREGATOR = 7,
		COMMUNITY = 8,
		ORIGINATOR_ID = 9,
		CLUSTER_LIST = 10,
		DPA = 11, //deprecated
		ADVERTISER = 12, //deprecated
		CLUSTER_ID = 13, //deprecated
		MP_REACH_NLRI = 14,
		MP_UNREACH_NLRI = 15,
		EXTENDED_COMMUNITY = 16,
		AS4_PATH = 17,
		AS4_AGGREGATOR = 18,
		SSA = 19, //deprecated
		Connector_Attribute = 20, //deprecated
		AS_PATHLIMIT = 21, //deprecated
		PMSI_TUNNEL = 22,
		Tunnel_Encapsulation_Attribute = 23,
		Traffic_Engineering = 24,
		IPv6_Extended_Community = 25,
		AIGP = 26,
		PE_Distinguisher_Labels = 27,
		BGP_Entropy_Label_Capability = 28, //deprecated
		BGP_LS = 29,
		LARGE_COMMUNITY = 32,
		BGPsec_Path = 33,
		ATTR_SET = 128
#pragma warning restore CA1707
	}
}
