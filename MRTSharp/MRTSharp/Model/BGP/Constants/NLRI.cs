﻿namespace MRTSharp.Model.BGP.Constants
{
	public enum AFI
	{
		IPv4 = 1,
		IPv6 = 2,
		L2VPN = 25
	}

	public enum SAFI
	{
#pragma warning disable CA1707 // This are identifier as used in the RFC
		UNICAST = 1,
		MULTICAST = 2,
		UNICAST_MULTICAST = 3,
		MPLS_LABEL = 4,
		EVPN = 70,
		MPLS_LABEL_VPN = 128
#pragma warning restore CA1707
	}
}
