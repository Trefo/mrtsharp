﻿using MRTSharp.Comparers;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.IP;
using System;
using System.Collections.Generic;
using System.Net;

namespace MRTSharp.Model.BGP
{
	public class BGPMessageUpdate : BGPMessage, IEquatable<BGPMessageUpdate>
	{ 
		public List<IPPrefix> Withdrawals { get; set; }
		public List<IPPrefix> Announcements { get; set; }
		public BGPOriginType Origin { get; set; }
		public List<ASEntry> ASPath { get; set; }
		public List<IPAddress> NextHops { get; set; }
		public UInt32? Med { get; set; }
		public UInt32? LocalPreference { get; internal set; }
		public bool AtomicAggregate { get; set; }
		public BGPAggregator Aggregator { get; set; }
		public List<IBGPCommunity> Communities { get; set; }
		public IPAddress OriginatorID { get; set; }
		public IPAddress[] ClusterList { get; set; }
		public BGPASPathLimit? AsPathLimit { get; set; }
		public Tuple<UInt32, BGPMessageUpdate> AttrSet { get; set; }

		private readonly static ListEqualityComparer<IPPrefix> PrefixComparer = new();
		private readonly static ListEqualityComparer<IPAddress> AddressComparer = new();
		private readonly static ArrayEqualityComparer<IPAddress> ArrayAddressComparer = new();
		private readonly static ListEqualityComparer<ASEntry> ASEntryComparer = new();
		private readonly static ListEqualityComparer<IBGPCommunity> CommunityComparer = new();

		#region What is and what is not
		public bool ContainsWithdrawals()
		{
			return (Withdrawals?.Count ?? 0) > 0;
		}

		public bool ContainsAnnouncements()
		{
			return (Announcements?.Count ?? 0) > 0;
		}
		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			return Equals(obj as BGPMessageUpdate);
		}

		public bool Equals(BGPMessageUpdate other)
		{
			// If parameter is null, return false.
			if (other is null)
			{
				return false;
			}

			// Optimization for a common success case.
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return GetType() == other.GetType() &&
				   base.Equals(other) &&
				   PrefixComparer.Equals(Withdrawals, other.Withdrawals) &&
				   PrefixComparer.Equals(Announcements, other.Announcements) &&
				   Origin == other.Origin &&
				   ASEntryComparer.Equals(ASPath, other.ASPath) &&
				   AddressComparer.Equals(NextHops, other.NextHops) &&
				   (Med?.Equals(other.Med) ?? true) &&
				   (LocalPreference?.Equals(other.LocalPreference) ?? true) &&
				   AtomicAggregate == other.AtomicAggregate &&
				   (ReferenceEquals(Aggregator, other.Aggregator) || Aggregator.Equals(other.Aggregator)) &&
				   CommunityComparer.Equals(Communities, other.Communities) &&
				   (OriginatorID?.Equals(other.OriginatorID) ?? true) &&
				   ArrayAddressComparer.Equals(ClusterList, other.ClusterList) &&
				   AsPathLimit.Equals(other.AsPathLimit);
		}

		public override int GetHashCode()
		{
			HashCode hash = new();
			hash.Add(base.GetHashCode());
			hash.Add(PrefixComparer.GetHashCode(Withdrawals));
			hash.Add(PrefixComparer.GetHashCode(Announcements));
			hash.Add(Origin);
			hash.Add(ASEntryComparer.GetHashCode(ASPath));
			hash.Add(AddressComparer.GetHashCode(NextHops));
			hash.Add(Med);
			hash.Add(LocalPreference);
			hash.Add(AtomicAggregate);
			hash.Add(Aggregator);
			hash.Add(CommunityComparer.GetHashCode(Communities));
			hash.Add(OriginatorID);
			hash.Add(ArrayAddressComparer.GetHashCode(ClusterList));
			hash.Add(AsPathLimit);
			return hash.ToHashCode();
		}
		#endregion
	}
}
