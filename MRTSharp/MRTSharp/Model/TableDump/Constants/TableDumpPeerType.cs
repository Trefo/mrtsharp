﻿namespace MRTSharp.Model.TableDump.Constants
{
	// See https://tools.ietf.org/html/rfc6396#section-4.3.1
	public enum TableDumpPeerType
	{
#pragma warning disable CA1707
		IPV4_2BYTE = 0,
		IPV6_2BYTE = 1,
		IPV4_4BYTE = 2,
		IPV6_4BYTE = 3
#pragma warning restore CA1707
	}
}
