﻿using MRTSharp.Model.IP;
using MRTSharp.Model.MRT;
using MRTSharp.Model.TableDump.Constants;
using System;
using System.Collections.Generic;

namespace MRTSharp.Model.TableDump
{
	public class TableDumpRIBRecord : MRTEntry
	{
		public TableDumpSubtype Subtype { get; private set; }
		public IPPrefix Prefix { get; set; }

		public List<TableDumpRIBEntry> Entries { get; } = new List<TableDumpRIBEntry>();

		public TableDumpRIBRecord(TableDumpSubtype subtype, IPPrefix prefix)
		{
			Subtype = subtype;
			Prefix = prefix;
		}

		public Boolean HasAddPath()
		{
			return Subtype == TableDumpSubtype.RIB_IPV4_UNICAST_ADDPATH ||
					Subtype == TableDumpSubtype.RIB_IPV6_MULTICAST_ADDPATH ||
					Subtype == TableDumpSubtype.RIB_IPV6_UNICAST_ADDPATH ||
					Subtype == TableDumpSubtype.RIB_IPV6_MULTICAST_ADDPATH;
		}
	}
}
