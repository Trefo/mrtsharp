﻿using Microsoft.Extensions.Logging;
using MRTSharp.AppContext;
using MRTSharp.File;
using MRTSharp.Model.MRT;
using MRTSharp.Tasks.File;
using MRTSharp.Tasks.MRT;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MRTSharp
{
	public static class MRTParser
	{
		public static void SetLogger(ILogger logger)
		{
			Singleton<ApplicationContext>.Instance.Logger = logger;
		}

		public static List<MRTEntry> Run(FileInfo fileInfo)
		{
			List<MRTEntry> entries = new();

			Run<MRTEntry>(fileInfo, (entry) => entries.Add(entry));

			return entries;
		}

		public static List<MRTEntry> Run(IEnumerable<FileInfo> fileInfos, Boolean multithreaded = true)
		{
			Object addLock = new();
			List<MRTEntry> entries = new();

			if (multithreaded)
			{
				Run<MRTEntry>(fileInfos, (entry) =>
				{
					lock (addLock)
					{
						entries.Add(entry);
					}
				}, multithreaded);
			}
			else
			{
				Run<MRTEntry>(fileInfos, (entry) =>	entries.Add(entry), multithreaded);
			}

			return entries;
		}

		public static List<MRTEntry> Run(DirectoryInfo directoryInfo, Boolean multithreaded = true)
		{
			Object addLock = new();
			List<MRTEntry> entries = new();

			if (multithreaded)
			{
				Run<MRTEntry>(directoryInfo, (entry) =>
				{
					lock (addLock)
					{
						entries.Add(entry);
					}
				}, multithreaded);
			}
			else
			{
				Run<MRTEntry>(directoryInfo, (entry) => entries.Add(entry), multithreaded);
			}

			return entries;
		}

		public static List<T> Run<T>(FileInfo fileInfo) where T : MRTEntry
		{
			List<T> entries = new();

			Run<T>(fileInfo, (entry) => entries.Add(entry));

			return entries;
		}

		public static List<T> Run<T>(IEnumerable<FileInfo> fileInfos, Boolean multithreaded = true) where T : MRTEntry
		{
			Object addLock = new();
			List<T> entries = new();

			if (multithreaded)
			{
				Run<T>(fileInfos, (entry) =>
				{
					lock (addLock)
					{
						entries.Add(entry);
					}
				}, multithreaded);
			}
			else
			{
				Run<T>(fileInfos, (entry) => entries.Add(entry), multithreaded);
			}

			return entries;
		}

		public static List<T> Run<T>(DirectoryInfo directoryInfo, Boolean multithreaded = true) where T : MRTEntry
		{
			Object addLock = new();
			List<T> entries = new();

			if (multithreaded)
			{
				Run<T>(directoryInfo, (entry) =>
				{
					lock (addLock)
					{
						entries.Add(entry);
					}
				}, multithreaded);
			}
			else
			{
				Run<T>(directoryInfo, (entry) => entries.Add(entry), multithreaded);
			}

			return entries;
		}

		public static void Run(FileInfo fileInfo, Action<MRTEntry> callback)
		{
			Run<MRTEntry>(fileInfo, callback);
		}

		public static void Run(IEnumerable<FileInfo> fileInfos, Action<MRTEntry> callback, Boolean multithreaded = true)
		{
			Run<MRTEntry>(fileInfos, callback, multithreaded);
		}

		public static void Run(DirectoryInfo directoryInfo, Action<MRTEntry> callback, Boolean multithreaded = true)
		{
			if (directoryInfo is null)
			{
				throw new ArgumentNullException(nameof(directoryInfo));
			}

			if (!directoryInfo.Exists)
			{
				throw new FileNotFoundException($"Directory {directoryInfo.FullName} does not exist");
			}

			FileInfo[] fileInfos = directoryInfo.GetFiles("", SearchOption.TopDirectoryOnly);

			Run<MRTEntry>(fileInfos, callback, multithreaded);
		}

		public static void Run<T>(FileInfo fileInfo, Action<T> callback) where T : MRTEntry
		{
			if (fileInfo is null)
			{
				throw new ArgumentNullException(nameof(fileInfo));
			}

			if ((fileInfo.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
			{
				throw new FileNotFoundException($"{fileInfo.FullName} is a directory");
			}

			if (!fileInfo.Exists)
			{
				throw new FileNotFoundException($"File {fileInfo.FullName} does not exist");
			}

			ReadAndParse(fileInfo, entry =>
			{
				if (entry is not T typedEntry)
				{
					return;
				}

				callback(typedEntry);
			});
		}

		public static void Run<T>(IEnumerable<FileInfo> fileInfos, Action<T> callback, Boolean multithreaded = true) where T : MRTEntry
		{
			if (fileInfos is null)
			{
				throw new ArgumentNullException(nameof(fileInfos));
			}

			if (multithreaded)
			{
				RunMultiThreaded<T>(fileInfos, callback);
			}
			else
			{
				RunSingleThreaded<T>(fileInfos, callback);
			}
		}

		public static void Run<T>(DirectoryInfo directoryInfo, Action<T> callback, Boolean multithreaded = true) where T : MRTEntry
		{
			if (directoryInfo is null)
			{
				throw new ArgumentNullException(nameof(directoryInfo));
			}

			if (!directoryInfo.Exists)
			{
				throw new FileNotFoundException($"Directory {directoryInfo.FullName} does not exist");
			}

			FileInfo[] fileInfos = directoryInfo.GetFiles("", SearchOption.TopDirectoryOnly);

			Run<T>(fileInfos, callback, multithreaded);
		}

		private static void RunSingleThreaded<T>(IEnumerable<FileInfo> fileInfos, Action<T> callback)
		{
			foreach (FileInfo fileInfo in fileInfos)
			{
				ReadAndParse(fileInfo, entry =>
				{
					if (entry is not T typedEntry)
					{
						return;
					}

					callback(typedEntry);
				});
			}
		}

		private static void RunMultiThreaded<T>(IEnumerable<FileInfo> fileInfos, Action<T> callback)
		{
			Parallel.ForEach(fileInfos, fileInfo =>
			{
				ReadAndParse(fileInfo, entry =>
				{
					if (entry is not T typedEntry)
					{
						return;
					}
					callback(typedEntry);
				});
			});
		}

		private static void ReadAndParse(FileInfo fileInfo, Action<MRTEntry> callback)
		{
			try
			{
				FileBuffer buffer = FileReaderTask.Run(fileInfo);

				(new MRTTask(buffer)).Start(callback);
			}
			catch (Exception ex)
			{
				Singleton<ApplicationContext>.Instance.Logger?.LogDebug(ex, "Corrupted file: {fileInfo}", fileInfo);
			}
		}
	}
}
