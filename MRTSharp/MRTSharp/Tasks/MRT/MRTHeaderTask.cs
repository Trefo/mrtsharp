﻿using MRTSharp.File;
using MRTSharp.Model.MRT;
using MRTSharp.Model.MRT.Constants;
using System;
using System.Runtime.InteropServices;

namespace MRTSharp.Tasks.MRT
{
	internal static class MRTHeaderTask
	{
		private static readonly int HeaderLength = Marshal.SizeOf<MRTHeader>();

		internal static MRTHeader Run(FileBuffer buffer)
		{
			MRTHeader header = buffer.ReadAsType<MRTHeader>(HeaderLength)[0];

			if (!IsExtendedType(header.GetMRTType()))
			{
				header.SetMicrosecondTimestamp(0);
				buffer.Seek(-sizeof(UInt32));
			}

			return header;
		}

		private static Boolean IsExtendedType(MRTType type)
		{
			return type == MRTType.BGP4MP_ET ||
					type == MRTType.ISIS_ET ||
					type == MRTType.OSPFv3_ET;
		}
	}
}
