﻿using MRTSharp.File;
using MRTSharp.Model.BGP.Constants;
using System;

namespace MRTSharp.Tasks.BGP.Common
{
	internal static class BGPAFISAFITask
	{
		public static (AFI, SAFI) Run(FileBuffer buffer, ref UInt16 length, Boolean skipReserved = false)
		{
			AFI afi = (AFI)buffer.ReadUInt16();
			length -= sizeof(UInt16);

			/* Skip Reserved if required */
			if (skipReserved)
			{
				buffer.Seek(sizeof(Byte));
				length -= sizeof(Byte);
			}

			SAFI safi = (SAFI)buffer.ReadByte();
			length -= sizeof(Byte);

			return (afi, safi);
		}
	}
}
