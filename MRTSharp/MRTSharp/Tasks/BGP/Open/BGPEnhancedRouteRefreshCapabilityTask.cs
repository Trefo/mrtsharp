﻿using MRTSharp.Model.BGP.Capabilities;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPEnhancedRouteRefreshCapabilityTask
	{
		internal static BGPEnhancedRouteRefreshCapability Run(UInt16 capabilityLength)
		{
			if (capabilityLength != 0)
			{
				throw new Exception("ENHANCED_ROUTE_REFRESH capability length malformed");
			}

			return new BGPEnhancedRouteRefreshCapability();
		}
	}
}
