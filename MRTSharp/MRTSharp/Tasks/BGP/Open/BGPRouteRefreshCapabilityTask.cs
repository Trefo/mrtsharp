﻿using MRTSharp.File;
using MRTSharp.Model.BGP.Capabilities;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPRouteRefreshCapabilityTask
	{
		internal static BGPRouteRefreshCapability Run(UInt16 capabilityLength)
		{
			if (capabilityLength != 0)
			{
				throw new Exception("ROUTE_REFRESH capability length malformed");
			}

			return new BGPRouteRefreshCapability();
		}
	}
}
