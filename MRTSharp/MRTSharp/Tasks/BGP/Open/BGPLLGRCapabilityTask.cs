﻿using MRTSharp.Model.BGP.Capabilities;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPLLGRCapabilityTask
	{
		internal static BGPLLGRCapability Run(UInt16 capabilityLength)
		{
			if (capabilityLength != 0)
			{
				throw new Exception("LLGR capability length malformed");
			}

			return new BGPLLGRCapability();
		}
	}
}
