﻿using MRTSharp.File;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Tasks.BGP.Common;
using System;

namespace MRTSharp.Tasks.BGP.Open
{
	internal static class BGPMultiProtocolCapabilityTask
	{
		private const int MultiProtocolCapabilityLength = 4;

		internal static BGPMultiProtocolCapability Run(FileBuffer buffer, UInt16 capabilityLength)
		{
			if (capabilityLength != MultiProtocolCapabilityLength)
			{
				throw new Exception("MULTIPROTOCOL capability length malformed");
			}

			(AFI afi, SAFI safi) = BGPAFISAFITask.Run(buffer, ref capabilityLength, skipReserved: true);

			return new BGPMultiProtocolCapability()
			{
				Afi = afi,
				Safi = safi
			};
		}
	}
}
