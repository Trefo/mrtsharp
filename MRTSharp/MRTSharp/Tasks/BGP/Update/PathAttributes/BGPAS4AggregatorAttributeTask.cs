﻿using MRTSharp.File;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPAS4AggregatorAttributeTask : BGPAbstractAggregatorAttributeTask
	{
		private const int AS4AggregatorLength = 8;

		internal override void TestLength(UInt16 attributeLength, Byte asByteLength)
		{
			if (attributeLength != AS4AggregatorLength)
			{
				throw new Exception("Aggregator field length malformed");
			}
		}

		internal override UInt32 ReadASNumber(FileBuffer buffer, UInt16 attributeLength)
		{
			return buffer.ReadUInt32();
		}
	}
}
