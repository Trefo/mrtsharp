﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.IP;
using System;
using System.Collections.Generic;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPUnreachNLRIAttributeTask : BGPAbstractNLRIAttributeTask
	{
		internal override void ParseNextHops(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, ref UInt16 attributeLength, (AFI afi, SAFI safi) afiSafi, UInt16 flags) 
		{
			/* No next hops in UNREACH_NLRI */
		}

		internal override List<IPPrefix> GetNLRIList(ref BGPMessageUpdate bgpUpdate, ushort attributeLength)
		{
			if (attributeLength > 0 && bgpUpdate.Withdrawals is null)
			{
				bgpUpdate.Withdrawals = new List<IPPrefix>();
			}

			return bgpUpdate.Withdrawals;
		}
	}
}
