﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;
using System.Net;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPClusterListAttributeTask : IBGPAttributeTask
	{
		private const int ClusterIdLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength % ClusterIdLength != 0)
			{
				throw new Exception("CLUSTER_LIST field length malformed");
			}

			int nClusterIds = attributeLength / ClusterIdLength;
			if (nClusterIds > 0)
			{
				bgpUpdate.ClusterList ??= new IPAddress[nClusterIds];

				for (int i = 0; i < nClusterIds; ++i)
				{
					bgpUpdate.ClusterList[i] = IPUtils.ReadIPAddress(buffer, IPType.IPv4);
				}
			}
		}
	}
}
