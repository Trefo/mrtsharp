﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal abstract class BGPAbstractASPathAttributeTask : IBGPAttributeTask
	{
		protected Byte? AttributeAsByteLength;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			List<ASEntry> asPath = new List<ASEntry>();

			Byte asByteLength = FlagUtils.GetASByteLength(flags);
			/* If it is AS4_PATH class, overwrite the parameter with the proper value */
			asByteLength = AttributeAsByteLength ?? asByteLength;

			while (attributeLength > 0)
			{
				ASPathSegmentType type = (ASPathSegmentType)buffer.ReadByte();
				UInt16 asPathSegmentLength = (UInt16)(buffer.ReadByte() * asByteLength);

				if (!IsASPathValid(bgpUpdate, attributeLength, type, asPathSegmentLength))
				{
					buffer.Seek(attributeLength - sizeof(Byte) - sizeof(Byte));

					bgpUpdate.Malformed = true;
					bgpUpdate.ASPath = asPath;

					return;
				}

				ASEntry entry = new ASEntry
				{
					SegmentType = type
				};

				switch (asByteLength)
				{
					case 2:
						ReadOnlySpan<UInt16> asNumbers = buffer.ReadAsType<UInt16>(asPathSegmentLength);

						entry.ASNumbers = new UInt32[asNumbers.Length];
						for (int i = 0; i < asNumbers.Length; ++i)
						{
							entry.ASNumbers[i] = (UInt32)ByteUtils.Swap(asNumbers[i]);
						}

						break;

					case 4:
						entry.ASNumbers = buffer.ReadAsType<UInt32>(asPathSegmentLength).ToArray();

						for (int i = 0; i < entry.ASNumbers.Length; ++i)
						{
							entry.ASNumbers[i] = ByteUtils.Swap(entry.ASNumbers[i]);
						}

						break;
				}

				asPath.Add(entry);

				attributeLength -= sizeof(Byte) + sizeof(Byte);
				attributeLength -= asPathSegmentLength;
			}

			bgpUpdate.ASPath = asPath;
		}

		internal virtual Boolean IsASPathValid(BGPMessageUpdate bgpUpdate, UInt16 attributeLength, ASPathSegmentType type, UInt16 asPathSegmentLength)
		{
			return (type == ASPathSegmentType.AS_SEQUENCE || type == ASPathSegmentType.AS_SET) && asPathSegmentLength <= attributeLength;
		}
	}
}
