﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPAttrSetAttributeTask : IBGPAttributeTask
	{
		private const int AttrSetMinLength = 4;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength < AttrSetMinLength)
			{
				throw new Exception("ATTR_SET field malformed");
			}

			UInt32 originAS = buffer.ReadUInt32();
			attributeLength -= sizeof(UInt32);

			BGPMessageUpdate encapsulatedBgpMessageUpdate = new BGPMessageUpdate();

			BGP4MPMessageUpdateAttributesTask.ParseAttributes(buffer, ref encapsulatedBgpMessageUpdate, attributeLength, flags);

			bgpUpdate.AttrSet = new Tuple<uint, BGPMessageUpdate>(originAS, encapsulatedBgpMessageUpdate);
		}
	}
}
