﻿using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using System;

namespace MRTSharp.Tasks.BGP.Update.PathAttributes
{
	internal class BGPOriginAttributeTask : IBGPAttributeTask
	{
		private const int OriginLength = 1;

		public void Run(FileBuffer buffer, ref BGPMessageUpdate bgpUpdate, UInt16 attributeLength, UInt16 flags)
		{
			if (attributeLength != OriginLength)
			{
				throw new Exception("Origin field length malformed");
			}

			bgpUpdate.Origin = (BGPOriginType)buffer.ReadByte();
		}
	}
}
