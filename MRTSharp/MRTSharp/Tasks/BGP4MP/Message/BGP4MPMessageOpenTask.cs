﻿using Microsoft.Extensions.Logging;
using MRTSharp.AppContext;
using MRTSharp.File;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.Capabilities;
using MRTSharp.Model.BGP.Constants;
using MRTSharp.Model.BGP4MP;
using MRTSharp.Model.IP;
using MRTSharp.Tasks.BGP.Open;
using MRTSharp.Utils;
using System;

namespace MRTSharp.Tasks.BGP4MP.Message
{
	internal static class BGP4MPMessageOpenTask
	{
		internal static void Run(FileBuffer buffer, BGP4MPMessage bgp4MPMessage)
		{
			BGPMessageOpen bgpMessageOpen = new BGPMessageOpen()
			{
				BGPVersion = buffer.ReadByte(),
				ASNumber = buffer.ReadUInt16(),
				HoldTime = TimeSpan.FromSeconds(buffer.ReadUInt16()),
				BGPIdentifier = IPUtils.ReadIPAddress(buffer, IPType.IPv4)
			};

			bgp4MPMessage.Message = bgpMessageOpen;

			Byte totalParametersLength = buffer.ReadByte();
			while (totalParametersLength > 0)
			{
				BGPParameterType type = (BGPParameterType)buffer.ReadByte();
				Byte bgpParameterLength = buffer.ReadByte();

				totalParametersLength -= (sizeof(Byte) + sizeof(Byte)); /* type + parameterLength */

				if (type != BGPParameterType.CAPABILITY)
					throw new Exception($"OPEN PARAMETER_TYPE {type} not defined");

				while (bgpParameterLength > 0)
				{
					BGPCapability capabilityCode = (BGPCapability)buffer.ReadByte();
					Byte capabilityLength = buffer.ReadByte();

					bgpMessageOpen.Capabilities.Add(
						(capabilityCode) switch
						{
							BGPCapability.MULTIPROTOCOL => BGPMultiProtocolCapabilityTask.Run(buffer, capabilityLength),
							BGPCapability.ROUTE_REFRESH => BGPRouteRefreshCapabilityTask.Run(capabilityLength),
							BGPCapability.GRACEFUL_RESTART => BGPGracefulRestartCapabilityTask.Run(buffer, capabilityLength),
							BGPCapability.SUPPORT_4_ASN => BGPAS4SupportCapabilityTask.Run(buffer, capabilityLength, bgpMessageOpen),
							BGPCapability.ENHANCED_ROUTE_REFRESH => BGPEnhancedRouteRefreshCapabilityTask.Run(capabilityLength),
							BGPCapability.LLGR => BGPLLGRCapabilityTask.Run(capabilityLength),
							_ => new Func<IBGPCapability>(() =>
							{
								Singleton<ApplicationContext>.Instance.Logger?.LogDebug("Capability {capabilityCode} not parsed", capabilityCode);

								buffer.Seek(capabilityLength);

								return null;
							}).Invoke()
						}
					);

					bgpParameterLength -= capabilityLength;
					bgpParameterLength -= (sizeof(Byte) + sizeof(Byte)); /* capabilityCode + capabilityLength */

					totalParametersLength -= capabilityLength;
					totalParametersLength -= (sizeof(Byte) + sizeof(Byte)); /* capabilityCode + capabilityLength */
				}
			}
		}
	}
}
