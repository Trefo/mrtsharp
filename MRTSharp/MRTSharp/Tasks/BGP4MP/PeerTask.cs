﻿using MRTSharp.File;
using MRTSharp.Model.BGP4MP.Peer;
using MRTSharp.Model.BGP4MP.Peer.Raw;
using MRTSharp.Model.IP;
using MRTSharp.Utils;
using System;
using System.Runtime.InteropServices;

namespace MRTSharp.Tasks.BGP4MP
{
	internal static class PeerTask
	{
		internal static BGPPeer Run(FileBuffer buffer, uint asLength, IPType ipType)
		{
			IBGPPeer peers = asLength switch
			{
				2 => ReadPeer<BGPPeer2>(buffer),
				4 => ReadPeer<BGPPeer4>(buffer),
				_ => throw new Exception("Error in AS Length")
			};

			BGPPeer bgpPeer = new BGPPeer()
			{
				Peers = peers
			};

			if (ipType != IPType.Null)
			{
				bgpPeer.PeerIPAddress = IPUtils.ReadIPAddress(buffer, ipType);
				bgpPeer.LocalPeerIPAddress = IPUtils.ReadIPAddress(buffer, ipType);
			}
			else
			{
				/* There is no Interface Index and AFI, so we seek back of 4 bytes and set both fields to 0 */
				peers.SetInterfaceNumber(0);
				peers.SetIPType(0);
				buffer.Seek(-sizeof(UInt16) - sizeof(UInt16));
			}

			return bgpPeer;
		}

		private static IBGPPeer ReadPeer<T>(FileBuffer buffer) where T : struct, IBGPPeer
		{
			int peerSize = Marshal.SizeOf<T>();

			return buffer.ReadAsType<T>(peerSize)[0];
		}
	}
}
