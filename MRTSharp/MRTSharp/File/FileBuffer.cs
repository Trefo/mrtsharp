﻿using MRTSharp.Utils;
using System;
using System.Buffers;
using System.IO;
using System.Runtime.InteropServices;

namespace MRTSharp.File
{
	internal class FileBuffer
	{
		private readonly ReadOnlySequence<Byte> _buffer;
		private long _position;

		private const int _maxSize = 0x7FFFFFC7;

		public FileBuffer(ReadOnlySequence<Byte> buffer)
		{
			_buffer = buffer;
		}

		public uint ReadUInt32()
		{
			ReadOnlySpan<byte> span = GetBytes(4);

			return unchecked(
				 (((uint)span[0]) << 24) | (((uint)span[1]) << 16) | (((uint)span[2]) << 8) | (((uint)span[3]) << 0)
				 );
		}
		public UInt16 ReadUInt16()
		{
			ReadOnlySpan<byte> span = GetBytes(2);

			return (UInt16)unchecked(
				 (((uint)span[0]) << 8) | (((uint)span[1]) << 0)
				 );
		}

		public byte ReadByte()
		{
			ReadOnlySpan<byte> span = GetBytes(1);
			return span[0];
		}

		public ReadOnlySpan<byte> ReadBytes(int length)
		{
			return GetBytes(length);
		}

		public ReadOnlySpan<T> ReadAsType<T>(int length) where T : struct
		{
			ReadOnlySpan<byte> span = GetBytes(length);

			return MemoryMarshal.Cast<byte, T>(span);
		}

		public bool CheckLength()
		{
			if (_position <= _buffer.Length)
			{
				return true;
			}

			return false;
		}

		public bool CheckAvailableLength()
		{
			if (_position < _buffer.Length)
			{
				return true;
			}

			return false;
		}

		public void Seek(int offset)
		{
			_position += offset;
			if (!CheckLength())
			{
				throw new IndexOutOfRangeException();
			}
		}

		private ReadOnlySpan<byte> GetBytes(int length)
		{
			if (!_buffer.IsSingleSegment)
			{
				SequenceReader<Byte> sequenceReader = new(_buffer);
				sequenceReader.Advance(_position);

				Seek(length);

				ReadOnlySequence<Byte> chunk = sequenceReader.Sequence.Slice(_position - length, length);
				if (chunk.IsSingleSegment)
				{
					return chunk.FirstSpan;
				}
				else
				{
					/* Data is between two spans, this is the only time we need to copy in a new span */
					Span<Byte> bufferSpan = new(new Byte[length]);
					chunk.CopyTo(bufferSpan);

					return bufferSpan;
				}
			}
			else
			{
				Seek(length);
				return _buffer.FirstSpan.Slice((int)(_position - length), length);
			}
		}

		public static FileBuffer FromStream(Stream stream, long? length = null)
		{
			if (stream is null)
			{
				throw new ArgumentNullException(nameof(stream));
			}

			byte[] chunkBuffer;
			MemorySegment<Byte> first;
			MemorySegment<Byte> current;

			if (length is not null && length.Value <= _maxSize)
			{
				/* Length is know, read directly */
				chunkBuffer = new byte[length.Value];

				ReadByteChunk(stream, chunkBuffer);

				first = new MemorySegment<Byte>(chunkBuffer);
				current = first;
			}
			else
			{
				/* Length is not known, read chunks */
				chunkBuffer = new byte[_maxSize];

				int totalRead = ReadByteChunk(stream, chunkBuffer);

				first = new MemorySegment<Byte>(chunkBuffer);

				if (totalRead < _maxSize)
				{
					first = new MemorySegment<Byte>(first.Memory[..totalRead]);
					current = first;
				}
				else
				{
					current = first;

					while (totalRead > 0)
					{
						chunkBuffer = new byte[_maxSize];
						totalRead = ReadByteChunk(stream, chunkBuffer);

						if (totalRead == 0)
						{
							break;
						}

						MemorySegment<Byte> tempSegment = new(chunkBuffer);

						if (totalRead < _maxSize)
						{
							tempSegment = new MemorySegment<Byte>(tempSegment.Memory[..totalRead]);
						}

						current = current.Append(tempSegment.Memory);
					}
				}
			}

			return new FileBuffer(new ReadOnlySequence<Byte>(first, 0, current, current.Memory.Length));
		}

		private static int ReadByteChunk(Stream stream, byte[] chunkBuffer)
		{
			int totalRead = 0;
			while (totalRead < chunkBuffer.Length)
			{
				int bytesRead = stream.Read(chunkBuffer, totalRead, chunkBuffer.Length - totalRead);
				if (bytesRead == 0) break;
				totalRead += bytesRead;
			}

			return totalRead;
		}

		public static FileBuffer FromBytes(byte[] stream)
		{
			MemorySegment<Byte> first = new(stream);

			return new FileBuffer(new ReadOnlySequence<Byte>(first, 0, first, first.Memory.Length));
		}
	}
}
