﻿using System;

namespace MRTSharp.Utils
{
	internal class FlagUtils
	{
		public static UInt16 GetFlags(Byte asByteLength, Boolean addPath, Boolean skipNLRI)
		{
			UInt16 flags = asByteLength;
			flags = (UInt16)(flags << 4);
			flags |= (Byte)(addPath ? 8 : 0);
			flags |= (Byte)(skipNLRI ? 4 : 0);

			return flags;
		}

		public static Byte GetASByteLength(UInt16 flags)
		{
			return (Byte)((flags & 0xf0) >> 4);
		}

		public static Boolean GetAddPath(UInt16 flags)
		{
			return (flags & 0x08) == 8;
		}

		public static Boolean GetSkipNLRI(UInt16 flags)
		{
			return (flags & 0x04) == 4;
		}
	}
}
