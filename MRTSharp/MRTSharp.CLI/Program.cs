﻿using CommandLine;
using MRTSharp.CLI.Loaders;
using MRTSharp.CLI.Model;
using MRTSharp.CLI.OutputSerializers;
using MRTSharp.CLI.OutputStreams;
using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MRTSharp.CLI
{
	class Program
	{
		static void Main(string[] args)
		{
			Parser.Default.ParseArguments<CommandLineOptions>(args)
			  .WithParsed(RunOptions)
			  .WithNotParsed(HandleParseError);
		}
		private static void RunOptions(CommandLineOptions opts)
		{
			opts.FormatPaths();

			IEnumerable<string> invalidPaths = opts.GetInvalidPaths();

			if (invalidPaths.Any())
			{
				Console.Error.WriteLine($"Paths {string.Join(", ", invalidPaths)} not valid.");
				return;
			}

			IEnumerable<FileInfo> filesOrdered = FileLoader.LoadFiles(opts.Path);

			IOutputStream outputStream = GetOutputStream(opts);
			outputStream.Open();

			IOutputSerializer outputSerializer = GetOutputSerializers(opts.Format);

			MRTParser.Run<BGP4MPMessage>(filesOrdered, (entry) =>
			{
				if (entry.Message is BGPMessageUpdate typedEntry)
				{
					if (opts.Multithreaded)
					{
						outputStream.SafeWrite(outputSerializer.Serialize(entry, typedEntry));
					}
					else
					{
						outputStream.Write(outputSerializer.Serialize(entry, typedEntry));
					}
				}
			}, multithreaded: opts.Multithreaded);


			outputStream.Close();
		}

		private static IOutputSerializer GetOutputSerializers(Format format)
		{
			return format switch
			{
				Format.json => new JsonOutputSerializer(),
				Format.tsv => new TsvOutputSerializer(),
				_ => throw new ArgumentException("Output format not defined"),
			};
		}

		private static IOutputStream GetOutputStream(CommandLineOptions opts)
		{
			if (opts.OutputFile is not null)
			{
				return new FileOutputStream(opts.OutputFile);
			}
			if (opts.StdOut)
			{
				return new ConsoleOutputStream();
			}
			if (opts.Socket is not null)
			{
				return new SocketOutputStream(opts.Socket);
			}

			throw new ArgumentException("Output not defined");
		}

		static void HandleParseError(IEnumerable<Error> errs)
		{
			Console.Error.WriteLine("Arguments missing");
		}
	}
}
