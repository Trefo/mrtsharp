﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP4MP;
using System;

namespace MRTSharp.CLI.OutputSerializers
{
	public interface IOutputSerializer
	{
		public Byte[] Serialize(BGP4MPMessage entry, BGPMessageUpdate data);
	}
}
