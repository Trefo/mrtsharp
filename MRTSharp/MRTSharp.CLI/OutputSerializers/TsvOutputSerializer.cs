﻿using MRTSharp.Model.BGP;
using MRTSharp.Model.BGP.PathAttributes;
using MRTSharp.Model.BGP4MP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRTSharp.CLI.OutputSerializers
{
	public class TsvOutputSerializer : IOutputSerializer
	{
		public byte[] Serialize(BGP4MPMessage entry, BGPMessageUpdate data)
		{
			String output = "";

			if (data.Announcements is not null)
			{
				foreach (var prefix in data.Announcements)
				{
					output += $"BGP4MP|{((DateTimeOffset)entry.OriginateTime).ToUnixTimeSeconds()}|A|{entry.Peers.PeerIPAddress}|{entry.Peers.Peers.GetPeerAS()}|{prefix}|" +
						$"{SerializeAsPath(data.ASPath)}|{data.Origin}|{string.Join(" ", data.NextHops)}|" +
						$"{data.LocalPreference ?? 0}|{(data.Med ?? 0)}|{string.Join(" ", data.Communities ?? Enumerable.Empty<IBGPCommunity>())}|" +
						$"{(data.AtomicAggregate ? "AG" : "NAG")}|{data.Aggregator}|\n";
				}
			}

			if (data.Withdrawals is not null)
			{
				foreach (var prefix in data.Withdrawals)
				{
					output += $"BGP4MP|{((DateTimeOffset)entry.OriginateTime).ToUnixTimeSeconds()}|W|{entry.Peers.PeerIPAddress}|{entry.Peers.Peers.GetPeerAS()}|{prefix}\n";
				}
			}

			return Encoding.UTF8.GetBytes(output);
		}

		private static String SerializeAsPath(List<ASEntry> aSPath)
		{
			StringBuilder output = new();
			foreach (var entry in aSPath)
			{
				if (entry.SegmentType == ASPathSegmentType.AS_SET)
				{
					output.Append("{" + String.Join(",", entry.ASNumbers) + "}");
				}
				else
				{
					output.Append(String.Join(" ", entry.ASNumbers));
				}
			}
			return output.ToString();
		}
	}
}
