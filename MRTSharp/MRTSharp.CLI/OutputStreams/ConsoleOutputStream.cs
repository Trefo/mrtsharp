﻿using System;
using System.Text;

namespace MRTSharp.CLI.OutputStreams
{
	public class ConsoleOutputStream : IOutputStream
	{
		public void Close() { }

		public void Open() { }

		public void SafeWrite(byte[] line)
		{
			Write(line);
		}

		public void Write(byte[] line)
		{
			Console.Write(Encoding.UTF8.GetString(line));
		}
	}
}
