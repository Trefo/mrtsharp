﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MRTSharp.CLI.Loaders
{
	public class FileLoader
	{
		internal static IEnumerable<FileInfo> LoadFiles(IEnumerable<string> path)
		{
			IEnumerable<FileInfo> files = path.Where(x => System.IO.File.Exists(x)).Select(x => new FileInfo(x));

			files = files.Concat(path.Where(x => Directory.Exists(x)).SelectMany(x => new DirectoryInfo(x).GetFiles("", SearchOption.TopDirectoryOnly)));

			return files.OrderBy(x => x.Name);
		}
	}
}